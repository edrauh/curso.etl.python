# CURSO:  ASSISTENTE DE ANALISTA DE DADOS  - II

Este repositório será utilizado para que você aluno, possa realizar suas entregas (commmits) 
a partir de uma versão base(release_1.0). 


## curso.etl.python

# Criando a sua Branch 
utilize a release_1.0 para criar a sua branch seguindo o exemplo:

feature/nome-aluno-exercicio-1.

### Comandos utilizados para o versionamento de arquivos (códigos):

Acesse o diretório onde você irá criar o seu projeto:
```
cd diretório do seu execício.

```

Adicione o repositório remoto:
```
git remote add origin https://gitlab.com/edrauh/curso.etl.python.git
```

Mude de branch da Main para release_1.0
```
git checkout release_1.0
```
Crie a sua branch para subir os seus códigos
```
git branch -M feature/nome-aluno-exercicio-1

git push -uf origin feature/nome-aluno-exercicio-1
```

Ajuda - [ Tutorial sobre linhas de comando GIT ](https://www.freecodecamp.org/portuguese/news/10-comandos-do-git-que-todo-desenvolvedor-deveria-conhecer/)

Ajuda - [Configurações e preferências do Git no Visual Studio](https://learn.microsoft.com/pt-br/visualstudio/version-control/git-settings?view=vs-2022)

